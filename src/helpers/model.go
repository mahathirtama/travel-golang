package helpers

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/responses"
)

func ToCityResponse(city entities.City) responses.CityResponse {
	return responses.CityResponse{
		Id: int(city.ID),
		Name: city.Name,
	}
}

func ToCityResponses(cities []entities.City) []responses.CityResponse {
	var cityResponses []responses.CityResponse
	for _, city := range cities {
		cityResponses = append(cityResponses, ToCityResponse(city))
	}
	return cityResponses
}