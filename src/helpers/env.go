package helpers

import "os"

func Env( key string,ifNotFound interface{})interface{}{
	env,ok:=os.LookupEnv(key)
	if !ok{
		return ifNotFound
	}else{
		return env
	}
}