package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type TripAcomodationRepository struct {
	c *config.Config
}

func NewTripAcomodationRepository(c *config.Config) *TripAcomodationRepository {
	return &TripAcomodationRepository{
		c,
	}
}

func (r *TripAcomodationRepository) Create(request *entities.TripAcomodation) *entities.TripAcomodation {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *TripAcomodationRepository) Delete(id string) *entities.TripAcomodation {
	db,sql,err := r.c.GetDBFunction().Connect()
	var tripAcomodation *entities.TripAcomodation
	err = db.First(&tripAcomodation, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	err = db.Delete(&tripAcomodation, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return tripAcomodation
}