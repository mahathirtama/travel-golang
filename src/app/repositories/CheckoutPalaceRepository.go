package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type CheckoutPalaceRepository struct {
	c *config.Config
}

func NewCheckoutPalaceRepository(c *config.Config) *CheckoutPalaceRepository {
	return &CheckoutPalaceRepository{
		c,
	}
}

func (r *CheckoutPalaceRepository) Create(request *entities.CheckoutPalace) *entities.CheckoutPalace {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *CheckoutPalaceRepository) GetAll() []entities.CheckoutPalace {
	db,sql,err := r.c.GetDBFunction().Connect()
	var checkoutPalace []entities.CheckoutPalace
	err = db.Find(&checkoutPalace).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return checkoutPalace
}

func (r *CheckoutPalaceRepository) GetOne(id string) *entities.CheckoutPalace {
	db,sql,err := r.c.GetDBFunction().Connect()
	var checkoutPalace *entities.CheckoutPalace
	err = db.First(&checkoutPalace, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return checkoutPalace
}

func (r *CheckoutPalaceRepository) Update(id string, request *entities.CheckoutPalace) *entities.CheckoutPalace {
	db,sql,err := r.c.GetDBFunction().Connect()
	var checkoutPalace *entities.CheckoutPalace
	err = db.First(&checkoutPalace, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	checkoutPalace.UserId = request.UserId
	checkoutPalace.PalaceId = request.PalaceId
	checkoutPalace.TransactionNumber = request.TransactionNumber
	checkoutPalace.TotalPrice = request.TotalPrice
	checkoutPalace.Qty = request.Qty
	checkoutPalace.Firstname = request.Firstname
	checkoutPalace.Lastname = request.Lastname
	checkoutPalace.Email = request.Email
	checkoutPalace.Phone = request.Phone

	err = db.Save(checkoutPalace).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return checkoutPalace
	
}

func (r *CheckoutPalaceRepository) Delete(id string) *entities.CheckoutPalace {
	db,sql,err := r.c.GetDBFunction().Connect()
	var checkoutPalace *entities.CheckoutPalace
	err = db.First(&checkoutPalace, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	err = db.Delete(&checkoutPalace, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return checkoutPalace
}
