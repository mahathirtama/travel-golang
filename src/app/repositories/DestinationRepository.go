package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type DestinationRepository struct {
	c *config.Config
}

func NewDestinationRepository(c *config.Config) *DestinationRepository {
	return &DestinationRepository{
		c,
	}
}

func (r *DestinationRepository) Create(request *entities.Destination) *entities.Destination {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *DestinationRepository) GetAll() []entities.Destination {
	db,sql,err := r.c.GetDBFunction().Connect()
	var destination []entities.Destination
	err = db.Preload("Tag").Preload("Country").Preload("City").Preload("Province").Find(&destination).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return destination

}

func (r *DestinationRepository) GetOne(id string) *entities.Destination {
	db,sql,err := r.c.GetDBFunction().Connect()
	var destination *entities.Destination
	err = db.First(&destination, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return destination
}

func (r *DestinationRepository) Update(id string, request *entities.Destination) *entities.Destination {
	db,sql,err := r.c.GetDBFunction().Connect()
	var destination *entities.Destination
	err = db.First(&destination, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	destination.UserId = request.UserId
	destination.TagId = request.TagId
	destination.CountryId = request.CountryId
	destination.CityId = request.CityId
	destination.ProvinceId = request.ProvinceId
	destination.DestinationName = request.DestinationName
	destination.Image = request.Image
	destination.Price = request.Price
	destination.Description = request.Description
	err = db.Save(destination).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return destination
	
}

func (r *DestinationRepository) Delete(id string) *entities.Destination {
	db,sql,err := r.c.GetDBFunction().Connect()
	var destination *entities.Destination
	err = db.First(&destination, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	err = db.Delete(&destination, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return destination
}