package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type OAuthTokenRepository struct {
	c *config.Config
}

func NewOAuthTokenRepository(c *config.Config) *OAuthTokenRepository {
	return &OAuthTokenRepository{
		c,
	}
}

func (r *OAuthTokenRepository) Create(request *entities.OAuthToken) *entities.OAuthToken {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *OAuthTokenRepository) GetOne(column string, anyOne string) (string,error) {
	db,sql,err := r.c.GetDBFunction().Connect()
	var oAuthToken *entities.OAuthToken
	query := column + "=?"
	err = db.Where(query, anyOne).First(&oAuthToken).Error
	if err != nil {
		return "", err
	}
	defer sql.Close()
	helpers.PanicIfError(err)

	var level *entities.User
	err = db.First(&level, oAuthToken.UserId).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return level.Level,err
}

func (r *OAuthTokenRepository) Update(id string, token string) (*entities.OAuthToken,error) {
	db,sql,err := r.c.GetDBFunction().Connect()
	var oauth *entities.OAuthToken
	err = db.First(&oauth, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	oauth.Token = token
	err = db.Save(oauth).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	if err != nil {
		return nil, err
	}
	return oauth, err
	
}

func (r *OAuthTokenRepository) Delete(token string) *entities.OAuthToken {
	db,sql,err := r.c.GetDBFunction().Connect()
	var oAuthToken *entities.OAuthToken
	err = db.Delete(&oAuthToken, "token =?", token).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return oAuthToken
}