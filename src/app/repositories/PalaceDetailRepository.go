package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type PalaceDetailRepository struct {
	c *config.Config
}

func NewPalaceDetailRepository(c *config.Config) *PalaceDetailRepository {
	return &PalaceDetailRepository{
		c,
	}
}

func (r *PalaceDetailRepository) Create(request *entities.PalaceDetail) *entities.PalaceDetail {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *PalaceDetailRepository) GetAll() []entities.PalaceDetail {
	db,sql,err := r.c.GetDBFunction().Connect()
	var palaceDetail []entities.PalaceDetail
	err = db.Find(&palaceDetail).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return palaceDetail
}


func (r *PalaceDetailRepository) Delete(id string) *entities.PalaceDetail {
	db,sql,err := r.c.GetDBFunction().Connect()
	var palaceDetail *entities.PalaceDetail
	err = db.First(&palaceDetail, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	err = db.Delete(&palaceDetail, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return palaceDetail
}