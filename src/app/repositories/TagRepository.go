package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type TagRepository struct {
	c *config.Config
}

func NewTagRepository(c *config.Config) *TagRepository {
	return &TagRepository{
		c,
	}
}

func (r TagRepository) Create(request *entities.Tag) *entities.Tag {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r TagRepository) GetAll() []entities.Tag {
	db,sql,err := r.c.GetDBFunction().Connect()
	var tag []entities.Tag
	err = db.Find(&tag).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return tag
}