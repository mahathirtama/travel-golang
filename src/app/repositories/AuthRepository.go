package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/dto"
	"travel-golang/src/helpers"
)

type AuthRepository struct {
	c *config.Config
}

func NewAuthRepository(c *config.Config) *AuthRepository {
	return &AuthRepository{
		c,
	}
}

func (r *AuthRepository) Create(request *entities.User) *entities.User {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *AuthRepository) Login(request *dto.LoginDto) *entities.User {
	db,sql,err := r.c.GetDBFunction().Connect()
	var login *entities.User
	err = db.Where("username=?",request.Username).First(&login).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return login
}