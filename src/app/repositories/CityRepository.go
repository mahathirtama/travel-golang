package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type CityRepository struct {
	c *config.Config
}

func NewCItyRepository(c *config.Config) *CityRepository {
	return &CityRepository{
		c,
	}
}

func (r *CityRepository) Create( request *entities.City) (*entities.City, error) {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request,err
} 

func (r *CityRepository) GetAll() []entities.City {
	db,sql,err := r.c.GetDBFunction().Connect()
	var cities []entities.City
	err = db.Find(&cities).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return cities
}



