package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type PalaceRepository struct {
	c *config.Config
}

func NewPalaceRepository(c *config.Config) *PalaceRepository {
	return &PalaceRepository{
		c,
	}
}

func (r *PalaceRepository) Create(request *entities.Palace) *entities.Palace {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *PalaceRepository) GetAll() []entities.Palace {
	db,sql,err := r.c.GetDBFunction().Connect()
	var palace []entities.Palace
	err = db.Preload("Tag").Preload("Country").Preload("City").Preload("Province").Find(&palace).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return palace

}

func (r *PalaceRepository) GetOne(id string) *entities.Palace {
	db,sql,err := r.c.GetDBFunction().Connect()
	var palace *entities.Palace
	err = db.First(&palace, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return palace
}

func (r *PalaceRepository) Update(id string, request *entities.Palace) *entities.Palace {
	db,sql,err := r.c.GetDBFunction().Connect()
	var palace *entities.Palace
	err = db.First(&palace, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	palace.UserId = request.UserId
	palace.TagId = request.TagId
	palace.CountryId = request.CountryId
	palace.CityId = request.CityId
	palace.ProvinceId = request.ProvinceId
	palace.PalaceName = request.PalaceName
	palace.Image = request.Image
	palace.Price = request.Price
	palace.Description = request.Description
	err = db.Save(palace).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return palace
	
}

func (r *PalaceRepository) Delete(id string) *entities.Palace {
	db,sql,err := r.c.GetDBFunction().Connect()
	var palace *entities.Palace
	err = db.First(&palace, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	err = db.Delete(&palace, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return palace
}