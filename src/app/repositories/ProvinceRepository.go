package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type ProvinceRepository struct {
	c *config.Config
}

func NewProvinceRepository(c *config.Config) *ProvinceRepository {
	return &ProvinceRepository{
		c,
	}
}

func (r *ProvinceRepository) Create(request *entities.Province) *entities.Province {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *ProvinceRepository) GetALl() []entities.Province {
	db,sql,err := r.c.GetDBFunction().Connect()
	var provinces []entities.Province
	err = db.Find(&provinces).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return provinces
}