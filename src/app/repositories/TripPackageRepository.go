package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type TripPackageRepository struct {
	c *config.Config
}

func NewTripPackageRepository(c *config.Config) *TripPackageRepository {
	return &TripPackageRepository{
		c,
	}
}

func (r *TripPackageRepository) Create(request *entities.TripPackage) *entities.TripPackage {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *TripPackageRepository) GetAll() []entities.TripPackage {
	db,sql,err := r.c.GetDBFunction().Connect()
	var tripPackage []entities.TripPackage
	err = db.Find(&tripPackage).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return tripPackage
}

func (r *TripPackageRepository) GetOne(id string) *entities.TripPackage {
	db,sql,err := r.c.GetDBFunction().Connect()
	var tripPackage *entities.TripPackage
	err = db.First(&tripPackage, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return tripPackage
}

func (r *TripPackageRepository) Update(id string, request *entities.TripPackage) *entities.TripPackage {
	db,sql,err := r.c.GetDBFunction().Connect()
	var tripPackage *entities.TripPackage
	err = db.First(&tripPackage, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	tripPackage.UserId = request.UserId
	tripPackage.DestinationId = request.DestinationId
	tripPackage.GuideId = request.GuideId
	tripPackage.Type = request.Type
	tripPackage.Duration = request.Duration
	tripPackage.Quota = request.Quota
	tripPackage.DepartureTime = request.DepartureTime
	err = db.Save(tripPackage).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return tripPackage
	
}

func (r *TripPackageRepository) Delete(id string) *entities.TripPackage {
	db,sql,err := r.c.GetDBFunction().Connect()
	var tripPackage *entities.TripPackage
	err = db.First(&tripPackage, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	err = db.Delete(&tripPackage, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return tripPackage
}
