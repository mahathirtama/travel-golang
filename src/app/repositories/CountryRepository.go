package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type CountryRepository struct {
	c *config.Config
}

func NewCountryRepository(c *config.Config) *CountryRepository {
	return &CountryRepository{
		c,
	}
}

func (r *CountryRepository) Create(request *entities.Country) *entities.Country {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *CountryRepository) GetAll() []entities.Country {
	db,sql,err := r.c.GetDBFunction().Connect()
	var countries []entities.Country
	err = db.Find(&countries).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return countries
}