package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type GuideRepository struct {
	c *config.Config 
}

func NewGuideRepository(c *config.Config) *GuideRepository {
	return &GuideRepository{
		c,
	}
}

func (r *GuideRepository) Create(request *entities.Guide) *entities.Guide {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *GuideRepository) GetAll() []entities.Guide {
	db,sql,err := r.c.GetDBFunction().Connect()
	var guides []entities.Guide
	err = db.Find(&guides).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return guides
}

func (r *GuideRepository) GetOne(id string) *entities.Guide {
	db,sql,err := r.c.GetDBFunction().Connect()
	var guides *entities.Guide
	err = db.First(&guides, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return guides
}

func (r *GuideRepository) Update(id string, request *entities.Guide) *entities.Guide {
	db,sql,err := r.c.GetDBFunction().Connect()
	var guides *entities.Guide
	err = db.First(&guides, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	guides.Name = request.Name
	err = db.Save(guides).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return guides
	
}

func (r *GuideRepository) Delete(id string) *entities.Guide {
	db,sql,err := r.c.GetDBFunction().Connect()
	var guides *entities.Guide
	err = db.First(&guides, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	err = db.Delete(&guides, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return guides
}