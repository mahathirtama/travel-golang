package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type CheckoutTripPackageRepository struct {
	c *config.Config
}

func NewCheckoutTripPackageRepository(c *config.Config) *CheckoutTripPackageRepository {
	return &CheckoutTripPackageRepository{
		c,
	}
}

func (r *CheckoutTripPackageRepository) Create(request *entities.CheckoutTripPackage) (*entities.CheckoutTripPackage, error) {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request, err
}

func (r *CheckoutTripPackageRepository) CreateDetailPackage(request *entities.PackageDetail) *entities.PackageDetail {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *CheckoutTripPackageRepository) FindDetailPackage(id string) ([]entities.PackageDetail, error) {
	db,sql,err := r.c.GetDBFunction().Connect()
	var packageDetail []entities.PackageDetail
	err = db.Where("trip_package_id =?", id).Find(&packageDetail).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return packageDetail, err
}

func (r *CheckoutTripPackageRepository) GetAll() []entities.CheckoutTripPackage {
	db,sql,err := r.c.GetDBFunction().Connect()
	var checkoutTripPackage []entities.CheckoutTripPackage
	err = db.Find(&checkoutTripPackage).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return checkoutTripPackage
}

func (r *CheckoutTripPackageRepository) GetOne(id string) *entities.CheckoutTripPackage {
	db,sql,err := r.c.GetDBFunction().Connect()
	var checkoutTripPackage *entities.CheckoutTripPackage
	err = db.First(&checkoutTripPackage, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return checkoutTripPackage
}


func (r *CheckoutTripPackageRepository) Update(id string, request *entities.CheckoutTripPackage) *entities.CheckoutTripPackage {
	db,sql,err := r.c.GetDBFunction().Connect()
	var checkoutTripPackage *entities.CheckoutTripPackage
	err = db.First(&checkoutTripPackage, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	checkoutTripPackage.UserId = request.UserId
	checkoutTripPackage.TripPackageId = request.TripPackageId
	checkoutTripPackage.TransactionNumber = request.TransactionNumber
	checkoutTripPackage.TotalPrice = request.TotalPrice
	checkoutTripPackage.Qty = request.Qty
	checkoutTripPackage.Firstname = request.Firstname
	checkoutTripPackage.Lastname = request.Lastname
	checkoutTripPackage.Email = request.Email
	checkoutTripPackage.Phone = request.Phone

	err = db.Save(checkoutTripPackage).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return checkoutTripPackage
	
}

func (r *CheckoutTripPackageRepository) Delete(id string) *entities.CheckoutTripPackage {
	db,sql,err := r.c.GetDBFunction().Connect()
	var checkoutTripPackage *entities.CheckoutTripPackage
	err = db.First(&checkoutTripPackage, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	err = db.Delete(&checkoutTripPackage, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return checkoutTripPackage
}