package repositories

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
	"travel-golang/src/helpers"
)

type DestinationDetailRepository struct {
	c *config.Config
}

func NewDestinationDetailRepository (c *config.Config) *DestinationDetailRepository {
	return &DestinationDetailRepository{
		c,
	}
}

func (r *DestinationDetailRepository) Create(request *entities.DestinationDetail) *entities.DestinationDetail {
	db,sql,err := r.c.GetDBFunction().Connect()
	err = db.Create(request).Error
	defer sql.Close()
	helpers.PanicIfError(err)

	return request
}

func (r *DestinationDetailRepository) GetAll() []entities.DestinationDetail {
	db,sql,err := r.c.GetDBFunction().Connect()
	var DestinationDetail []entities.DestinationDetail
	err = db.Find(&DestinationDetail).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return DestinationDetail
}

func (r *DestinationDetailRepository) Delete(id string) *entities.DestinationDetail {
	db,sql,err := r.c.GetDBFunction().Connect()
	var DestinationDetail *entities.DestinationDetail
	err = db.First(&DestinationDetail, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	err = db.Delete(&DestinationDetail, id).Error
	defer sql.Close()
	helpers.PanicIfError(err)
	return DestinationDetail
}