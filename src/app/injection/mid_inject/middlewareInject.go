//go:build wireinject
//+build wireinject

package midinject

import (
	tmiddleware "travel-golang/src/app/middleware"

	"github.com/google/wire"
)
func InitializeMiddlewareService() *tmiddleware.Middleware{
	wire.Build(
		tmiddleware.NewMiddleware,
	)
	return nil
}