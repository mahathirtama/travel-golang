//go:build wireinject
// +build wireinject
package countryinjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitCountry() *controllers.CountryControlller {
	wire.Build(
		config.NewConfig,
		repositories.NewCountryRepository,
		service.NewCountryService,
		controllers.NewCountryController,
	)

	return nil
}