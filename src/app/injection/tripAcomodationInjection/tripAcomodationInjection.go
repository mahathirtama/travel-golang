//go:build wireinject
// +build wireinject
package tripacomodationinjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitTripAcomodation() *controllers.TripAcomodationController {
	wire.Build(
		config.NewConfig,
		repositories.NewTripAcomodationRepository,
		service.NewTripAcomodationService,
		controllers.NewTripAcomodationController,
	)

	return nil
}