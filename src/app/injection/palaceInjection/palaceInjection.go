//go:build wireinject
// +build wireinject

package palaceinjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitPalace() *controllers.PalaceController {
	wire.Build(
		config.NewConfig,
		repositories.NewPalaceRepository,
		service.NewPalaceService,
		controllers.NewPalaceController,
	)
	return nil
}