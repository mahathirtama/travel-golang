//go:build wireinject
// +build wireinject

package destinationdetailinjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitDestinationDetail() *controllers.DestinationDetailController {
	wire.Build(
		config.NewConfig,
		repositories.NewDestinationDetailRepository,
		service.NewDestinationDetailService,
		controllers.NewDestinationDetailController,

	)
	return nil
}