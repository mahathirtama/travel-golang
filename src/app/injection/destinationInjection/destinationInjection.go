//go:build wireinject
// +build wireinject

package destinatoininejction

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitDestination() *controllers.DestinationController {
	wire.Build(
		config.NewConfig,
		repositories.NewDestinationRepository,
		service.NewDestinationService,
		controllers.NewDestinationController,

	)
	return nil
}