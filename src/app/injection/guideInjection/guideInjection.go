//go:build wireinject
// +build wireinject

package guideInjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitGuide() *controllers.GuideController {
	wire.Build(
		config.NewConfig,
		repositories.NewGuideRepository,
		service.NewGuideService,
		controllers.NewGuideController,

	)
	return nil
}