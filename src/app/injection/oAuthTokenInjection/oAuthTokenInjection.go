//go:build wireinject
// +build wireinject
package oauthtokeninjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitOAuthToken() *controllers.OAuthTokenController {
	wire.Build(
		config.NewConfig,
		repositories.NewOAuthTokenRepository,
		service.NewOAuthTokenService,
		controllers.NewOAuthTokenController,
	)
	return nil
}