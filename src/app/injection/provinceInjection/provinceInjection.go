//go:build wireinject
// +build wireinject
package provinceInjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitProvince() *controllers.ProviceController {
	wire.Build(
		config.NewConfig,
		repositories.NewProvinceRepository,
		service.NewProvinceService,
		controllers.NewProvinceController,
	)
	return nil
}