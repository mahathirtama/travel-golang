//go:build wireinject
// +build wireinject

package palacedetailinjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitPalaceDetail() *controllers.PalaceDetailController {
	wire.Build(
		config.NewConfig,
		repositories.NewPalaceDetailRepository,
		service.NewPalaceDetailService,
		controllers.NewPalaceDetailController,
	)
	return nil
}