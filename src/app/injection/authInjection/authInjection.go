//go:build wireinject
// +build wireinject
package authinjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitAuth() *controllers.AuthController {
	wire.Build(
		config.NewConfig,
		repositories.NewAuthRepository,
		repositories.NewOAuthTokenRepository,
		service.NewAuthService,
		controllers.NewAuthController,
		service.NewJWTService,
		service.NewOAuthTokenService,
	)
	return nil
}

