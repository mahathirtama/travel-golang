//go:build wireinject
// +build wireinject

package tagInjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitTag() *controllers.TagController {
	wire.Build(
		config.NewConfig,
		repositories.NewTagRepository,
		service.NewTagService,
		controllers.NewTagController,
	)
	return nil
}