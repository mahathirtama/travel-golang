//go:build wireinject
// +build wireinject
package checkoutpalaceinjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitCheckoutPalace() *controllers.CheckoutPalaceController {
	wire.Build(
		config.NewConfig,
		repositories.NewCheckoutPalaceRepository,
		service.NewCheckoutPalaceService,
		controllers.NewCheckoutPalaceController,
	)
	return nil
}