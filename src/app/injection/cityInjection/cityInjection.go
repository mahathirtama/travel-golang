//go:build wireinject
// +build wireinject
package cityInjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitCity() *controllers.CityController {
	wire.Build(
		config.NewConfig,
		repositories.NewCItyRepository,
		service.NewCityService,
		controllers.NewCityController,
	)

	return nil
}