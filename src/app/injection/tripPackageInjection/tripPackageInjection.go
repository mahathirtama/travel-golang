//go:build wireinject
// +build wireinject

package trippackageinjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitTripPackage() *controllers.TripPackageController {
	wire.Build(
		config.NewConfig,
		repositories.NewTripPackageRepository,
		service.NewTripPackageService,
		controllers.NewTripPackageController,
	)
	return nil
}