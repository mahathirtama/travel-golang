//go:build wireinject
// +build wireinject
package checkouttrippackageinjection

import (
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"
	"travel-golang/src/http/controllers"

	"github.com/google/wire"
)

func InitCheckoutTripPackage() *controllers.CheckoutTripPackageController {
	wire.Build(
		config.NewConfig,
		repositories.NewCheckoutTripPackageRepository,
		repositories.NewTripPackageRepository,
		service.NewCheckoutTripPackageService,
		controllers.NewCheckoutTripPackageController,
	)
	return nil
}