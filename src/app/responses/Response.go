package responses

type Response struct {
	Code   int         `json:"code"`
	Status string      `json:"status"`
	Data   interface{} `json:"data"`
}

type AuthResponse struct {
	Code   int         `json:"code"`
	Status string      `json:"status"`
	Token  string      `json:"token"`
	Data   interface{} `json:"data"`
}