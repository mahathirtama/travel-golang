package responses

type CityResponse struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}