package service

import (
	"fmt"
	"log"
	"net/http"
	"time"
	"travel-golang/src/app/responses"
	"travel-golang/src/dto"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
)

type JwtService struct {
	auth *AuthService
	oauth *OAuthTokenService
}

func (j *JwtService) GetAuth() *AuthService{
	return j.auth
}



func (j *JwtService ) Attempt(c echo.Context) error{
	var dtoRequest dto.LoginDto
    c.Bind(&dtoRequest)
	logger := log.Default()

	logger.Println(fmt.Sprintf("username:%v,password:%v",dtoRequest.Username,dtoRequest.Password))
	dtoData, payload := j.auth.Login(&dtoRequest)

	if err := bcrypt.CompareHashAndPassword([]byte(payload.Password), []byte(dtoRequest.Password)); err != nil {
		apiResponse := responses.Response{
		Code: 400,
		Status: "Password Incorrect",
		Data: nil,
	}
	return c.JSON(http.StatusBadRequest, apiResponse)
	}

	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
    claims["authorized"] = true
    claims["exp"] = time.Now().Add(time.Hour * 24).Unix() // Token berlaku selama 24 jam
	
	tokenString, err := token.SignedString([]byte("secret_key"))
    if err != nil {
			apiResponse := responses.Response{
			Code: 502,
			Status: "Token Can't Generate",
			Data: nil,
		}
		return c.JSON(http.StatusBadGateway, apiResponse)
	}
	str := fmt.Sprintf("%d", dtoData.Id)
		var _,errData = j.oauth.GetOne("id", str)
		if errData != nil {
			var tokenDto dto.OAuthTokenDto 
			tokenDto.Token = tokenString
			tokenDto.UserId = dtoData.Id
			tokenDto.Status = "Aktif"
			j.oauth.Create(tokenDto)
		}else {
			j.oauth.OAuthTokenRepository.Update(str, tokenString)
		}

	
	
	
	apiResponse := responses.AuthResponse{
		Code: 200,
		Status: "OK",
		Token: tokenString,
		Data: dtoData,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func NewJWTService(auth *AuthService,oauth *OAuthTokenService) *JwtService {
	return &JwtService{
		auth,oauth,
	}
}