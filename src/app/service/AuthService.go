package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
)

type AuthService struct {
	AuthRepository *repositories.AuthRepository
}

func NewAuthService(r *repositories.AuthRepository) *AuthService {
	return &AuthService{
		r,
	}
}

func (s *AuthService) Create(c echo.Context) *entities.User{
	dtoRequest := dto.AuthDto{}
	_ = c.Bind(&dtoRequest)

	var entity entities.User
	entity.Bind(dtoRequest)
	password, _ := bcrypt.GenerateFromPassword([]byte(entity.Password), 14)
	entity.Password = string(password)
	return s.AuthRepository.Create(&entity)
}

func (s *AuthService) Login(c *dto.LoginDto)( *dto.ResponseLoginUserDto, *entities.User){
	var dt dto.ResponseLoginUserDto
	  cari:= s.AuthRepository.Login(c)
	  dt.Id = cari.ID
	  dt.Username = cari.Username
	  dt.Phone = cari.Phone
	  dt.Email = cari.Email
	  dt.Level = cari.Level
	  return &dt, cari
}