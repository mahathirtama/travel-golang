package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type CityService struct {
	CityRepository *repositories.CityRepository
}

func NewCityService(r *repositories.CityRepository) *CityService{
	return &CityService{
		r,
	}
} 

func (s *CityService) Create(c echo.Context) (*entities.City, error){
	dtoRequest := dto.CityDto{}
	_ = c.Bind(&dtoRequest)

	var entity entities.City
	entity.Bind(dtoRequest)
	return s.CityRepository.Create(&entity)
}

func (s *CityService) GetAll() []entities.City {
	return s.CityRepository.GetAll() 
}