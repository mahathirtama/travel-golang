package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type DestinationService struct {
	DestinationRepository *repositories.DestinationRepository
}

func NewDestinationService (r *repositories.DestinationRepository) *DestinationService {
	return &DestinationService{
		r,
	}
}


func (s *DestinationService) Create(c echo.Context) *entities.Destination {
	dtoRequest := dto.DestinationDto{}
	_ = c.Bind(&dtoRequest)

	var entity entities.Destination
	entity.Bind(dtoRequest)
	return s.DestinationRepository.Create(&entity)
}

func (s *DestinationService) GetAll() []entities.Destination {
	return s.DestinationRepository.GetAll() 
}

func (s *DestinationService) GetOne(id string) *entities.Destination {
	return s.DestinationRepository.GetOne(id)
}

func (s *DestinationService) Update(id string, c echo.Context) *entities.Destination {
	dtoRequest := dto.DestinationDto{}
	_ = c.Bind(&dtoRequest)
	var entity entities.Destination
	entity.Bind(dtoRequest)
	return s.DestinationRepository.Update(id, &entity)
}

func (s *DestinationService) Delete(id string) *entities.Destination {
	return s.DestinationRepository.Delete(id)
}
