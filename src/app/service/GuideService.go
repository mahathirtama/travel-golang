package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type GuideService struct {
	GuideRepository *repositories.GuideRepository
}

func NewGuideService(r *repositories.GuideRepository) *GuideService {
	return &GuideService{
		r,
	}
}

func (s *GuideService) Create(c echo.Context) *entities.Guide {
	dtoRequest := dto.GuideDto{}
	_ = c.Bind(&dtoRequest)
	var entity entities.Guide
	entity.Bind(dtoRequest)
	return s.GuideRepository.Create(&entity)
}

func (s *GuideService) GetAll() []entities.Guide {
	return s.GuideRepository.GetAll()
}

func (s *GuideService) GetOne(id string) *entities.Guide {
	return s.GuideRepository.GetOne(id)
}

func (s *GuideService) Update(id string, c echo.Context) *entities.Guide {
	dtoRequest := dto.GuideDto{}
	_ = c.Bind(&dtoRequest)
	var entity entities.Guide
	entity.Bind(dtoRequest)
	return s.GuideRepository.Update(id, &entity)
}

func (s *GuideService) Delete(id string) *entities.Guide {
	return s.GuideRepository.Delete(id)
}