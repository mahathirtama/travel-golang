package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type TripAcomodationService struct {
	TripAcomodationRepository *repositories.TripAcomodationRepository
}

func NewTripAcomodationService(r *repositories.TripAcomodationRepository) *TripAcomodationService {
	return &TripAcomodationService{
		r,
	}
}

func (s *TripAcomodationService) Create(c echo.Context) *entities.TripAcomodation {
	dtoRequest := dto.TripAcomodationDto{}
	_ = c.Bind(&dtoRequest)

	var entity entities.TripAcomodation
	entity.Bind(dtoRequest)
	return s.TripAcomodationRepository.Create(&entity)
}

func (s *TripAcomodationService) Delete(id string) *entities.TripAcomodation {
	return s.TripAcomodationRepository.Delete(id)
}