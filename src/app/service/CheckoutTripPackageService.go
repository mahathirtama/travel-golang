package service

import (
	"fmt"
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type CheckoutTripPackageService struct {
	CheckoutTripPackageRepository *repositories.CheckoutTripPackageRepository
	TripPackageRepository *repositories.TripPackageRepository
}

func NewCheckoutTripPackageService(r *repositories.CheckoutTripPackageRepository, rt *repositories.TripPackageRepository) *CheckoutTripPackageService {
	return &CheckoutTripPackageService{
		r,rt,
	}
}

func (s *CheckoutTripPackageService) Create(c echo.Context) (*entities.CheckoutTripPackage, error) {
	dtoRequest := dto.CheckoutTripPackageDto{}
	_ = c.Bind(&dtoRequest)
	var entity entities.CheckoutTripPackage
	entity.Bind(dtoRequest)

	strTripPackage := fmt.Sprintf("%d", entity.TripPackageId)
	var tripPackage = s.TripPackageRepository.GetOne(strTripPackage)
	var detailTpkg ,err = s.CheckoutTripPackageRepository.FindDetailPackage(strTripPackage)

	if err != nil {
		return nil,err
	}

	var count int = len(detailTpkg)
	if count > 0 {
		var arrayQty []int
		for i := 0; i < count; i++ {
		idStr := fmt.Sprintf("%d", detailTpkg[i].CheckoutTripPackageId)
		var qty *entities.CheckoutTripPackage = s.CheckoutTripPackageRepository.GetOne(idStr)
		arrayQty = append(arrayQty, int(qty.Qty))
		}

		sum := 0
		 for _, num := range arrayQty {
        sum += num
    }
		if sum > int(tripPackage.Quota) {
			return &entity,err
		}
	}

	var res,error = s.CheckoutTripPackageRepository.Create(&entity)

	var packageDetail entities.PackageDetail
	packageDetail.UserId = res.UserId
	packageDetail.TripPackageId = res.TripPackageId
	packageDetail.CheckoutTripPackageId = res.ID
	s.CheckoutTripPackageRepository.CreateDetailPackage(&packageDetail)
	return res,error
}

func (s *CheckoutTripPackageService) GetAll() []entities.CheckoutTripPackage {
	return s.CheckoutTripPackageRepository.GetAll()
}

func (s *CheckoutTripPackageService) GetOne(id string) *entities.CheckoutTripPackage {
	return s.CheckoutTripPackageRepository.GetOne(id)
}

func (s *CheckoutTripPackageService) Update(id string, c echo.Context) *entities.CheckoutTripPackage {
	dtoRequest := dto.CheckoutTripPackageDto{}
	_ = c.Bind(&dtoRequest)
	var entity entities.CheckoutTripPackage
	entity.Bind(dtoRequest)
	return s.CheckoutTripPackageRepository.Update(id, &entity)
}

func (s *CheckoutTripPackageService) Delete(id string) *entities.CheckoutTripPackage {
	return s.CheckoutTripPackageRepository.Delete(id)
}