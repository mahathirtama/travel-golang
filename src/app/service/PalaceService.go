package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type PalaceService struct {
	PalaceRepository *repositories.PalaceRepository
}

func NewPalaceService(r *repositories.PalaceRepository) *PalaceService {
	return &PalaceService{
		r,
	}
}

func (s *PalaceService) Create(c echo.Context) *entities.Palace {
	dtoRequest := dto.PalaceDto{}
	_ = c.Bind(&dtoRequest)

	var entity entities.Palace
	entity.Bind(dtoRequest)
	return s.PalaceRepository.Create(&entity)
}

func (s *PalaceService) GetAll() []entities.Palace {
	return s.PalaceRepository.GetAll() 
}

func (s *PalaceService) GetOne(id string) *entities.Palace {
	return s.PalaceRepository.GetOne(id)
}

func (s *PalaceService) Update(id string, c echo.Context) *entities.Palace {
	dtoRequest := dto.PalaceDto{}
	_ = c.Bind(&dtoRequest)
	var entity entities.Palace
	entity.Bind(dtoRequest)
	return s.PalaceRepository.Update(id, &entity)
}

func (s *PalaceService) Delete(id string) *entities.Palace {
	return s.PalaceRepository.Delete(id)
}
