package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type CountryService struct {
	CountryRepository *repositories.CountryRepository
}

func NewCountryService(c *repositories.CountryRepository) *CountryService {
	return &CountryService{
		c,
	}
}

func (s *CountryService) Create(c echo.Context) *entities.Country {
	dtoRequest := dto.CountryDto{}
	_ = c.Bind(&dtoRequest)

	var entity entities.Country
	entity.Bind(dtoRequest)
	return s.CountryRepository.Create(&entity)
}

func (s *CountryService) GetAll() []entities.Country {
	return s.CountryRepository.GetAll()
}