package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type CheckoutPalaceService struct {
	CheckoutPalaceRepository *repositories.CheckoutPalaceRepository
}

func NewCheckoutPalaceService(r *repositories.CheckoutPalaceRepository) *CheckoutPalaceService {
	return &CheckoutPalaceService{
		r,
	}
}

func (s *CheckoutPalaceService) Create(c echo.Context) *entities.CheckoutPalace {
	dtoRequest := dto.CheckoutPalaceDto{}
	_ = c.Bind(&dtoRequest)
	var entity entities.CheckoutPalace
	entity.Bind(dtoRequest)
	return s.CheckoutPalaceRepository.Create(&entity)
}

func (s *CheckoutPalaceService) GetAll() []entities.CheckoutPalace {
	return s.CheckoutPalaceRepository.GetAll()
}

func (s *CheckoutPalaceService) GetOne(id string) *entities.CheckoutPalace {
	return s.CheckoutPalaceRepository.GetOne(id)
}

func (s *CheckoutPalaceService) Update(id string, c echo.Context) *entities.CheckoutPalace {
	dtoRequest := dto.CheckoutPalaceDto{}
	_ = c.Bind(&dtoRequest)
	var entity entities.CheckoutPalace
	entity.Bind(dtoRequest)
	return s.CheckoutPalaceRepository.Update(id, &entity)
}

func (s *CheckoutPalaceService) Delete(id string) *entities.CheckoutPalace {
	return s.CheckoutPalaceRepository.Delete(id)
}