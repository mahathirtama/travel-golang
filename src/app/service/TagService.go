package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type TagService struct {
	TagRepository *repositories.TagRepository
}

func NewTagService(r *repositories.TagRepository) *TagService {
	return &TagService{
		r,
	}
}

func (s *TagService) Create(c echo.Context) *entities.Tag {
	dtoRequest := dto.TagDto{}
	_ = c.Bind(&dtoRequest)

	var entity entities.Tag
	entity.Bind(dtoRequest)
	return s.TagRepository.Create(&entity)
}
func (s *TagService) GetAll() []entities.Tag {
	return s.TagRepository.GetAll()
}