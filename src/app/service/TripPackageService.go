package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type TripPackageService struct {
	TripPackageRepository *repositories.TripPackageRepository
}

func NewTripPackageService(r *repositories.TripPackageRepository) *TripPackageService {
	return &TripPackageService{
		r,
	}
}

func (s *TripPackageService) Create(c echo.Context) *entities.TripPackage {
	dtoRequest := dto.TripPackageDto{}
	_ = c.Bind(&dtoRequest)
	var entity entities.TripPackage
	entity.Bind(dtoRequest)
	return s.TripPackageRepository.Create(&entity)
}

func (s *TripPackageService) GetAll() []entities.TripPackage {
	return s.TripPackageRepository.GetAll()
}


func (s *TripPackageService) GetOne(id string) *entities.TripPackage {
	return s.TripPackageRepository.GetOne(id)
}

func (s *TripPackageService) Update(id string, c echo.Context) *entities.TripPackage {
	dtoRequest := dto.TripPackageDto{}
	_ = c.Bind(&dtoRequest)
	var entity entities.TripPackage
	entity.Bind(dtoRequest)
	return s.TripPackageRepository.Update(id, &entity)
}

func (s *TripPackageService) Delete(id string) *entities.TripPackage {
	return s.TripPackageRepository.Delete(id)
}
