package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type ProvinceService struct {
	ProvinceRepository *repositories.ProvinceRepository
}

func NewProvinceService(r *repositories.ProvinceRepository) *ProvinceService {
	return &ProvinceService{
		r,
	}
}

func (s *ProvinceService) Create(c echo.Context) *entities.Province {
	dtoRequest := dto.ProvinceDto{}
	_ = c.Bind(&dtoRequest)

	var entity entities.Province
	entity.Bind(dtoRequest)
	return s.ProvinceRepository.Create(&entity)
}

func (s *ProvinceService) GetAll() []entities.Province { 
	return s.ProvinceRepository.GetALl()
}