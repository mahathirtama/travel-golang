package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type DestinationDetailService struct {
	DestinationDetailRepository *repositories.DestinationDetailRepository
}

func NewDestinationDetailService(r *repositories.DestinationDetailRepository) *DestinationDetailService {
	return &DestinationDetailService{
		r,
	}
}

func (s *DestinationDetailService) Create(c echo.Context) *entities.DestinationDetail{
	dtoRequest := dto.DestinationDetailDto{}
	_ = c.Bind(&dtoRequest)
	var entity entities.DestinationDetail
	entity.Bind(dtoRequest)
	return s.DestinationDetailRepository.Create(&entity)
}


func (s *DestinationDetailService) GetAll() []entities.DestinationDetail {
	return s.DestinationDetailRepository.GetAll()
}


func (s *DestinationDetailService) Delete(id string) *entities.DestinationDetail {
	return s.DestinationDetailRepository.Delete(id)
}