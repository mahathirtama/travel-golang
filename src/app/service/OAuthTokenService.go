package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"
)

type OAuthTokenService struct {
	OAuthTokenRepository *repositories.OAuthTokenRepository
}

func NewOAuthTokenService(r *repositories.OAuthTokenRepository) *OAuthTokenService {
	return &OAuthTokenService{
		r,
	}
}

func (s *OAuthTokenService) Create(c dto.OAuthTokenDto) *entities.OAuthToken {
	var entity entities.OAuthToken
	entity.Bind(c)
	return s.OAuthTokenRepository.Create(&entity)
}

func (s *OAuthTokenService) GetOne(column string, anyOne string) (string,error) {
	return s.OAuthTokenRepository.GetOne(column, anyOne)
}

func (s *OAuthTokenService) Delete(token string) *entities.OAuthToken {
	return s.OAuthTokenRepository.Delete(token)
}