package service

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/app/repositories"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type PalaceDetailService struct {
	PalaceDetailRepository *repositories.PalaceDetailRepository
}

func NewPalaceDetailService(r *repositories.PalaceDetailRepository) *PalaceDetailService {
	return &PalaceDetailService{
		r,
	}
}

func (s *PalaceDetailService) Create(c echo.Context) *entities.PalaceDetail{
	dtoRequest := dto.PalaceDetailDto{}
	_ = c.Bind(&dtoRequest)
	var entity entities.PalaceDetail
	entity.Bind(dtoRequest)
	return s.PalaceDetailRepository.Create(&entity)
}


func (s *PalaceDetailService) GetAll() []entities.PalaceDetail {
	return s.PalaceDetailRepository.GetAll()
}

func (s *PalaceDetailService) Delete(id string) *entities.PalaceDetail {
	return s.PalaceDetailRepository.Delete(id)
}
