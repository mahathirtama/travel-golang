package tmiddleware

import (
	// "fmt"
	"fmt"
	"net/http"
	"strings"
	"travel-golang/src/app/repositories"
	"travel-golang/src/app/service"
	"travel-golang/src/config"

	// "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
)

type Middleware struct {
}

func NewMiddleware() *Middleware {
    return &Middleware{
        
    }
}

func  (m *Middleware) JwtMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
    return func(c echo.Context) error {
        // Extract the JWT token from the Authorization header
        authHeader := c.Request().Header.Get("Authorization")
        tokenString := strings.Split(authHeader, " ")[1] // Assuming the format is "Bearer <token>"
        var con config.Config = *config.NewConfig()
        var getRepository repositories.OAuthTokenRepository = *repositories.NewOAuthTokenRepository(&con)
        var getMiddleware service.OAuthTokenService = *service.NewOAuthTokenService(&getRepository)
        _,err:= getMiddleware.OAuthTokenRepository.GetOne("token", tokenString)
        

        if err != nil {
             return c.JSON(http.StatusUnauthorized, map[string]string{"error": "Forbidden"})
        }
        return next(c)
       
    }
}
func (m *Middleware)AdminMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
    return func(c echo.Context) error {
        // Extract the JWT token from the Authorization header
        authHeader := c.Request().Header.Get("Authorization")
        fmt.Printf("authHeader:%v",authHeader)
        tokenString := strings.Split(authHeader, " ")[1] // Assuming the format is "Bearer <token>"
        fmt.Printf("\ntokenString:%v",tokenString)
        var con config.Config = *config.NewConfig()
        fmt.Printf("\nconfig:%v",con)
        var getRepository repositories.OAuthTokenRepository = *repositories.NewOAuthTokenRepository(&con)
        var getMiddleware service.OAuthTokenService = *service.NewOAuthTokenService(&getRepository)
        level,err:=getMiddleware.OAuthTokenRepository.GetOne("token",tokenString)
        
        if err != nil {
            fmt.Printf("\nerr:%v",err.Error())
            return c.JSON(http.StatusUnauthorized, map[string]string{"error": "Forbidden"})
        }
        if level == "Admin" {
            return next(c)
        }else {
            return c.JSON(http.StatusUnauthorized, map[string]string{"error": "Forbidden User Didn't Admin"})
        }
        
    }
}

func (m *Middleware)TravelerMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
    return func(c echo.Context) error {
        // Extract the JWT token from the Authorization header
        authHeader := c.Request().Header.Get("Authorization")
        fmt.Printf("authHeader:%v",authHeader)
        tokenString := strings.Split(authHeader, " ")[1] // Assuming the format is "Bearer <token>"
        fmt.Printf("\ntokenString:%v",tokenString)
        var con config.Config = *config.NewConfig()
        fmt.Printf("\nconfig:%v",con)
        var getRepository repositories.OAuthTokenRepository = *repositories.NewOAuthTokenRepository(&con)
        var getMiddleware service.OAuthTokenService = *service.NewOAuthTokenService(&getRepository)
        level,err:=getMiddleware.OAuthTokenRepository.GetOne("token",tokenString)
        
        if err != nil {
            fmt.Printf("\nerr:%v",err.Error())
            return c.JSON(http.StatusUnauthorized, map[string]string{"error": "Forbidden"})
        }
        if level == "Traveler" {
            return next(c)
        }else {
            return c.JSON(http.StatusUnauthorized, map[string]string{"error": "Forbidden"})
        }
        
    }
}
