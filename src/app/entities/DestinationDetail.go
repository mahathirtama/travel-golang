package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type DestinationDetail struct {
	gorm.Model
	DestinationId uint `json:"destination_id" gorm:"column:destination_id"`
	Name string `json:"name" gorm:"column:name"`
	Image string `json:"image" gorm:"column:image"`
	Destination Destination `json:"destination" gorm:"foreignKey:DestinationId"`
}

func (c *DestinationDetail) Bind(d dto.DestinationDetailDto) {
	c.DestinationId = d.DestinationId
	c.Name = d.Name
	c.Image = d.Image
}