package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type Destination struct {
	gorm.Model
	UserId uint `json:"user_id" gorm:"column:user_id"`
	TagId uint `json:"tag_id" gorm:"column:tag_id"`
	CountryId uint `json:"country_id" gorm:"column:country_id"`
	CityId uint `json:"city_id" gorm:"column:city_id"`
	ProvinceId uint `json:"province_id" gorm:"column:province_id"`
	DestinationName string `json:"destination_name" gorm:"column:destination_name"`
	Image string `json:"image" gorm:"column:image"`
	Price uint `json:"price" gorm:"column:price"`
	Description string `json:"description" gorm:"column:description"`
	User User `json:"user" gorm:"foreignKey:UserId"`
	Tag Tag `json:"tag" gorm:"foreignKey:TagId"`
	Country Country `json:"country" gorm:"foreignKey:CountryId"`
	City City `json:"city" gorm:"foreignKey:CityId"`
	Province Province `json:"province" gorm:"foreignKey:ProvinceId"`
}

func (c *Destination) Bind(d dto.DestinationDto) {
	c.UserId = d.UserId
	c.TagId = d.TagId
	c.CountryId = d.CountryId
	c.CityId = d.CityId
	c.ProvinceId = d.ProvinceId
	c.DestinationName = d.DestinationName
	c.Image = d.Image
	c.Price = d.Price
	c.Description = d.Description
}