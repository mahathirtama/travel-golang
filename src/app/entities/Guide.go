package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type Guide struct {
	gorm.Model
	Name string `json:"name" gorm:"column:name`
}

func (c *Guide) Bind(d dto.GuideDto) {
	c.Name = d.Name
}