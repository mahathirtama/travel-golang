package entities

import "gorm.io/gorm"

type PackageDetail struct {
	gorm.Model
	UserId uint   `json:"user_id" gorm:"column:user_id"`
	TripPackageId uint `json:"trip_package_id" gorm:"column:trip_package_id"`
	CheckoutTripPackageId uint `json:"checkout_trip_package" gorm:"column:checkout_trip_package"`
	User User   `json:"user" gorm:"foreignKey:UserId"`
	TripPackage TripPackage `json:"trippackage" gorm:"foreignKey:TripPackageId"`
	CheckoutTripPackage CheckoutTripPackage `json:"checkout_trip_package" gorm:"foreignKey:CheckoutTripPackageId"`
	
}