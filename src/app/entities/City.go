package entities

import (
	"gorm.io/gorm"
	"travel-golang/src/dto"
)

type City struct {
	gorm.Model
	Name string `json:"name" gorm:"column:name`
}

func (c *City) Bind(d dto.CityDto) {
	c.Name = d.Name
}