package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type OAuthToken struct {
	gorm.Model
	UserId uint   `json:"user_id" gorm:"column:user_id`
	Token  string `json:"token" gorm:"column:token`
	Status string `json:"status" gorm:"column:status`
	User User `json:"user" gorm:"foreignKey:UserId"`
}

func (p *OAuthToken) Bind(d dto.OAuthTokenDto){
	p.UserId = d.UserId
	p.Token = d.Token
	p.Status = d.Status
}