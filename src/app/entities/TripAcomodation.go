package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type TripAcomodation struct {
	gorm.Model
	TripPackageId uint `json:"trip_package_id" gorm:"column:trip_package_id"`
	Name string `json:"name" gorm:"column:name"`
	TripPackage TripPackage `json:"trippackage" gorm:"foreignKey:TripPackageId"`
}

func (c *TripAcomodation) Bind(d dto.TripAcomodationDto) {
	c.TripPackageId = d.TripPackageId
	c.Name = d.Name
}