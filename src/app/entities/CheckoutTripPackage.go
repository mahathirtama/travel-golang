package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type CheckoutTripPackage struct {
	gorm.Model
	UserId            uint   `json:"user_id" gorm:"column:user_id"`
	TripPackageId	  uint `json:"trip_package_id" gorm:"column:trip_package_id"`
	TransactionNumber string `json:"transaction_number" gorm:"column:transaction_number"`
	TotalPrice        uint   `json:"total_price" gorm:"column:total_price"`
	Qty               uint   `json:"qty" gorm:"column:qty"`
	Firstname         string `json:"firstname" gorm:"column:firstname"`
	Lastname          string `json:"lastname" gorm:"column:lastname"`
	Email             string `json:"email" gorm:"column:email"`
	Phone             string `json:"phone" gorm:"column:phone"`
	User User   `json:"user" gorm:"foreignKey:UserId"`
	TripPackage TripPackage `json:"trippackage" gorm:"foreignKey:TripPackageId"`
	
}

func (c *CheckoutTripPackage) Bind(d dto.CheckoutTripPackageDto) {
	c.UserId = d.UserId
	c.TripPackageId = d.TripPackageId
	c.TransactionNumber = d.TransactionNumber
	c.TotalPrice = d.TotalPrice
	c.Qty = d.Qty
	c.Firstname = d.Firstname
	c.Lastname = d.Lastname
	c.Email = d.Email
	c.Phone = d.Phone
}