package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Username string `json:"username" gorm:"column:username`
	Password string `json:"password" gorm:"column:password`
	Email string `json:"email" gorm:"column:email`
	Phone string `json:"phone" gorm:"column:phone`
	Level string `json:"level" gorm:"column:level`
}


func (p *User) Bind(d dto.AuthDto){
	p.Username = d.Username
	p.Password = d.Password
	p.Email = d.Email
	p.Phone = d.Phone
	p.Level = d.Level
}