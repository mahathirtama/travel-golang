package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type Country struct {
	gorm.Model
	Name string `json:"name" gorm:"column:name`
}

func (c *Country) Bind(d dto.CountryDto) {
	c.Name = d.Name
}