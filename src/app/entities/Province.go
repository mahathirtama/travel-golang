package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type Province struct {
	gorm.Model
	Name string `json:"name" gorm:"column:name`
}

func (p *Province) Bind(d dto.ProvinceDto){
	p.Name = d.Name
}