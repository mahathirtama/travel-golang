package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type CheckoutPalace struct {
	gorm.Model
	UserId uint `json:"user_id" gorm:"column:user_id"`
	PalaceId uint `json:"palace_id" gorm:"column:palace_id"`
	TransactionNumber string `json:"transaction_number" gorm:"column:transaction_number"`
	TotalPrice uint `json:"total_price" gorm:"column:total_price"`
	Qty uint `json:"qty" gorm:"column:qty"`
	Firstname string `json:"firstname" gorm:"column:firstname"`
	Lastname string `json:"lastname" gorm:"column:lastname"`
	Email string `json:"email" gorm:"column:email"`
	Phone string `json:"phone" gorm:"column:phone"`
	User User `json:"user" gorm:"foreignKey:UserId"`
	Palace Palace `json:"palace" gorm:"foreignKey:PalaceId"`
}

func (c *CheckoutPalace) Bind(d dto.CheckoutPalaceDto) {
	c.UserId = d.UserId
	c.PalaceId = d.PalaceId
	c.TransactionNumber = d.TransactionNumber
	c.TotalPrice = d.TotalPrice
	c.Qty = d.Qty
	c.Firstname = d.Firstname
	c.Lastname = d.Lastname
	c.Email = d.Email
	c.Phone = d.Phone
}