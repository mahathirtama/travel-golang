package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type TripPackage struct {
	gorm.Model
	UserId uint `json:"user_id" gorm:"column:user_id"`
	DestinationId uint `json:"destination_id" gorm:"column:destination_id"`
	GuideId uint `json:"guide_id" gorm:"column:guide_id"`
	Type string `json:"type" gorm:"column:type"`
	Duration uint `json:"duration" gorm:"column:duration"`
	Quota uint `json:"quota" gorm:"column:quota"`
	DepartureTime string `json:"departure_time" gorm:"column:departure_time"`
	User User `json:"user" gorm:"foreignKey:UserId"`
	Destination Destination `json:"destination" gorm:"foreignKey:DestinationId"`
}

func (c *TripPackage) Bind(d dto.TripPackageDto) {
	c.UserId = d.UserId
	c.DestinationId = d.DestinationId
	c.GuideId = d.GuideId
	c.Type = d.Type
	c.Duration = d.Duration
	c.Quota = d.Quota
	c.DepartureTime = d.DepartureTime
}