package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type Tag struct {
	gorm.Model
	Name string `json:"name" gorm:"column:name`
}

func (p *Tag) Bind(d dto.TagDto){
	p.Name = d.Name
}