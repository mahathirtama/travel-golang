package entities

import (
	"travel-golang/src/dto"

	"gorm.io/gorm"
)

type PalaceDetail struct {
	gorm.Model
	PalaceId uint `json:"palace_id" gorm:"column:palace_id"`
	Image string `json:"image" gorm:"column:image"`
	Palace Palace `json:"palace" gorm:"foreignKey:PalaceId"`
}

func (c *PalaceDetail) Bind(d dto.PalaceDetailDto) {
	c.PalaceId = d.PalaceId
	c.Image = d.Image
}