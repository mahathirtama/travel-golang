package database

import (
	"database/sql"
	"travel-golang/src/helpers"
	"time"
)

func NewDB() *sql.DB {
	db, err := sql.Open("mysql", "postgres://postgres:tama123@localhost:5432/db_travel_golang")
	helpers.PanicIfError(err)

	db.SetMaxIdleConns(5)
	db.SetMaxOpenConns(20)
	db.SetConnMaxLifetime(60 * time.Minute)
	db.SetConnMaxIdleTime(10 * time.Minute)
	return db
}