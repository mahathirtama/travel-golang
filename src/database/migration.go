package database

import (
	"travel-golang/src/app/entities"
	"travel-golang/src/config"
)


func Migrate(){
	var c = config.NewConfig()
	var log = c.GetLogger()
	var dbf = c.GetDBFunction()
	db,sql,err := dbf.Connect()
	defer sql.Close()
	if err != nil {
		log.Info("error nih bos:"+err.Error())
	}
	db.Migrator().DropTable(&entities.User{},&entities.City{},&entities.Tag{}, &entities.Country{},&entities.Province{}, &entities.Palace{}, &entities.Guide{}, &entities.Destination{}, &entities.DestinationDetail{}, &entities.TripPackage{}, &entities.TripAcomodation{}, &entities.PalaceDetail{}, &entities.CheckoutPalace{}, &entities.CheckoutTripPackage{}, &entities.PackageDetail{}, &entities.OAuthToken{})
	db.Migrator().AutoMigrate(&entities.User{},&entities.City{},&entities.Tag{}, &entities.Country{},&entities.Province{}, &entities.Palace{}, &entities.Guide{}, &entities.Destination{}, &entities.DestinationDetail{}, &entities.TripPackage{}, &entities.TripAcomodation{}, &entities.PalaceDetail{}, &entities.CheckoutPalace{}, &entities.CheckoutTripPackage{}, &entities.PackageDetail{}, &entities.OAuthToken{})
}