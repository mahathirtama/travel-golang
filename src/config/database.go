package config

import (
	"database/sql"

	"gorm.io/gorm"
	"gorm.io/driver/postgres"
)



type DBFunctions struct{
	connectionString string
	log *LogApp
}


func (dbf *DBFunctions) Connect()(db *gorm.DB,sql *sql.DB, err error){
	db,err = gorm.Open( postgres.Open(dbf.connectionString),&gorm.Config{} )
	if err!=nil{
		return
	}
	sql,err = db.DB()
	if err != nil {
		return
	}
	return
}

func NewDBFunctions(constring string,log *LogApp) *DBFunctions{
	return &DBFunctions{constring,log}
}