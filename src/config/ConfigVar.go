package config

type ConfigVar struct {
	connectionString string
	appName string
}