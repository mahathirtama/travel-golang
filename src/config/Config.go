package config

import "travel-golang/src/helpers"

type Config struct {
	ConfigVar
	db  *DBFunctions
	log *LogApp
}

func (c *Config) InitEnvironment() {
	c.connectionString = helpers.Env("CONNECTION_STRING", "host=localhost user=postgres password=tama123 dbname=db_travel_golang port=5432 sslmode=disable").(string)
	c.appName = helpers.Env("APP_NAME", "travel-golang").(string)

}

func (c *Config) Init() {
	c.InitEnvironment()
}

func (c *Config) GetConnectionString() string {
	return c.connectionString
}
func (c *Config) GetDBFunction() *DBFunctions {
	return c.db
}
func (c *Config) GetLogger() *LogApp {
	return c.log
}

func NewConfig() *Config {
	var c = &Config{}
	c.Init()
	var log = NewLogApp(c.appName)
	c.log = log
	var db = NewDBFunctions(c.connectionString,log)
	c.db = db
	return c
}