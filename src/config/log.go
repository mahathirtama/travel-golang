package config

import (
	"fmt"
	"time"
)

type LogApp struct {
	appName string
}

func (l *LogApp) Info(message string) {
	var dateku string = time.Now().Format("2006-01-02 12:03:06")
	var psan = fmt.Sprintf("%s[%s] INFO : %s", l.appName, dateku, message)
	fmt.Println(psan)
}

func NewLogApp(appName string) *LogApp {
	return &LogApp{appName: appName}
}
