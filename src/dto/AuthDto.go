package dto

type AuthDto struct {
		Username string `validate:"required,min=1,max=100" json:"username"`
		Password string `validate:"required,min=1,max=100" json:"password"`
		Email string `validate:"required,min=1,max=100" json:"email"`
		Phone string `validate:"required,min=1,max=100" json:"phone"`
		Level string `validate:"required,min=1,max=100" json:"level"`
}

type LoginDto struct {
	Username string `validate:"required,min=1,max=100" json:"username"`
	Password string `validate:"required,min=1,max=100" json:"password"`
}

type ResponseUserDto struct {
		Username string `json:"username"`
		Email string `json:"email"`
		Phone string `json:"phone"`
		Level string `json:"level"`
}

type ResponseLoginUserDto struct {
		Id uint `json:"id"`
		Username string `validate:"required,min=1,max=100" json:"username"`
		Email string `validate:"required,min=1,max=100" json:"email"`
		Phone string `validate:"required,min=1,max=100" json:"phone"`
		Level string `validate:"required,min=1,max=100" json:"level"`
}
