package dto

type OAuthTokenDto struct {
	UserId uint `validate:"required" json:"user_id"`
	Token string `validate:"required" json:"token"`
	Status string `validate:"required" json:"status"`
}