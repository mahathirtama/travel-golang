package dto

type TripAcomodationDto struct {
	TripPackageId uint `validate:"required" json:"trip_package_id"`
	Name string `validate:"required,min=1,max=100" json:"name"`
}