package dto

type CheckoutTripPackageDto struct {
		UserId uint `validate:"required" json:"user_id"`
		TripPackageId uint `validate:"required" json:"trip_package_id"`
		TransactionNumber string `validate:"required,min=1,max=100" json:"transaction_number"`
		TotalPrice uint `validate:"required" json:"total_price"`
		Qty uint `validate:"required" json:"qty"`
		Firstname string `validate:"required,min=1,max=100" json:"firstname"`
		Lastname string `validate:"required,min=1,max=100" json:"lastname"`
		Email string `validate:"required,min=1,max=100" json:"email"`
		Phone string `validate:"required,min=1,max=100" json:"phone"`
}