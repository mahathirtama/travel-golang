package dto

type DestinationDetailDto struct {
	DestinationId uint `validate:"required" json:"destination_id"`
	Name string `validate:"required,min=1,max=100" json:"name"`
	Image string `validate:"required" json:"image"`
}