package dto

type TripPackageDto struct {
	UserId uint `validate:"required" json:"user_id"`
	DestinationId uint `validate:"required" json:"destination_id"`
	GuideId uint `validate:"required" json:"guide_id"`
	Type string `validate:"required" json:"type"`
	Duration uint `validate:"required" json:"duration"`
	Quota uint `validate:"required" json:"quota"`
	DepartureTime string `validate:"required" json:"departure_time"`
}