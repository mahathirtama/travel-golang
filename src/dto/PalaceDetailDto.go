package dto

type PalaceDetailDto struct {
	PalaceId uint `validate:"required" json:"palace_id"`
	Image string `validate:"required" json:"image"`
}