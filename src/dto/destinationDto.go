package dto

type DestinationDto struct {
	UserId uint `validate:"required" json:"user_id"`
	TagId uint `validate:"required" json:"tag_id"`
	CountryId uint `validate:"required" json:"country_id"`
	CityId uint `validate:"required" json:"city_id"`
	ProvinceId uint `validate:"required" json:"province_id"`
	DestinationName string `validate:"required,min=1,max=100" json:"palace_name"`
	Image string `validate:"required,min=1,max=100" json:"image"`
	Price uint `validate:"required" json:"price"`
	Description string `validate:"required,min=1,max=100" json:"description"`
}