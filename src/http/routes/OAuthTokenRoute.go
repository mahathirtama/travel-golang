package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/oAuthTokenInjection"

	"github.com/labstack/echo/v4"
)

type OAuthRoute struct {
}

func (c *OAuthRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitOAuthToken()
	group.Use(middleware.JwtMiddleware)
	group.DELETE("/logout", controller.DeleteOAuthToken)
}

func NewOAuthRouter()*OAuthRoute{
	return &OAuthRoute{}
}
