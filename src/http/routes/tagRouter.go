package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/tagInjection"

	"github.com/labstack/echo/v4"
)

type TagRoute struct {
}

func (c *TagRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitTag()
	group.Use(middleware.AdminMiddleware)
	group.GET("", controller.GetAllTag)
	group.POST("", controller.CreateTag)
}

func NewTagRouter()*TagRoute{
	return &TagRoute{}
}
