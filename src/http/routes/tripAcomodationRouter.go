package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/tripAcomodationInjection"

	"github.com/labstack/echo/v4"
)

type TripAcomodationRoute struct {
}

func (c *TripAcomodationRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitTripAcomodation()
	group.Use(middleware.AdminMiddleware)
	group.POST("", controller.CreateTripAcomodation)
	group.DELETE("", controller.DeleteTripAcomodation)
}

func NewTripAcomodationRouter()*TripAcomodationRoute{
	return &TripAcomodationRoute{}
}
