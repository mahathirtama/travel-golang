package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/destinationDetailInjection"

	"github.com/labstack/echo/v4"
)

type DestinationDetailRoute struct {
}

func (c *DestinationDetailRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitDestinationDetail()
	group.Use(middleware.AdminMiddleware)
	group.GET("", controller.GetAllDestinationDetail)
	group.POST("", controller.CreateDestinationDetail)
	group.DELETE("/:id", controller.DeleteDestinationDetail)
}

func NewDestinationDetailRoute()*DestinationDetailRoute{
	return &DestinationDetailRoute{}
}
