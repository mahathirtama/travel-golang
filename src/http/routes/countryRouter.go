package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/countryInjection"

	"github.com/labstack/echo/v4"
)

type CountryRoute struct {
}

func (c *CountryRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitCountry()
	group.Use(middleware.AdminMiddleware)
	group.GET("", controller.GetAllCountry)
	group.POST("", controller.CreateCountry)
}

func NewCountryRoute()*CountryRoute{
	return &CountryRoute{}
}
