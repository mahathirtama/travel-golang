package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/checkoutTripPackageInjection"

	"github.com/labstack/echo/v4"
)

type CheckoutTripPackageRoute struct {
}

func (c *CheckoutTripPackageRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitCheckoutTripPackage()
	group.Use(middleware.AdminMiddleware)
	group.GET("", controller.GetAllCheckoutTripPackage)
	group.PUT("/:id", controller.UpdateCheckoutTripPackage)
	group.GET("/:id", controller.GetOneCheckoutTripPackage)
	group.DELETE("/:id", controller.DeleteCheckoutTripPackage)
}

func NewCheckoutTripPackageRouter()*CheckoutTripPackageRoute{
	return &CheckoutTripPackageRoute{}
}