package routes

import (
	authinjection "travel-golang/src/app/injection/authInjection"
	"github.com/labstack/echo/v4"
)

func AuthRoute(e *echo.Echo) {
	controller := authinjection.InitAuth()
	e.POST("/api/register", controller.Register)
	e.POST("/api/login", controller.Login)
}