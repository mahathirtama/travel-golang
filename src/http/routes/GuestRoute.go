package routes

import (
	palaceinject "travel-golang/src/app/injection/palaceInjection"
	tripPackageinject "travel-golang/src/app/injection/tripPackageInjection"

	"github.com/labstack/echo/v4"
)

type GuestRoute struct {
}

func (c *GuestRoute) Router(group *echo.Group){ 
	var palaceinject = palaceinject.InitPalace()
	var tripPackageinject = tripPackageinject.InitTripPackage()
	group.GET("", palaceinject.GetAllPalace)
	group.GET("/:id", palaceinject.GetOnePalace)
	group.GET("", tripPackageinject.GetAllTripPackage)
	group.GET("/:id", tripPackageinject.GetOneTripPackage)
}

func NewGuestRoute()*GuestRoute{
	return &GuestRoute{}
}
