package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	palaceckt "travel-golang/src/app/injection/checkoutPalaceInjection"
	trippackageckt "travel-golang/src/app/injection/checkoutTripPackageInjection"

	"github.com/labstack/echo/v4"
)

type CheckoutRoute struct {
}

func (c *CheckoutRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var palaceckt = palaceckt.InitCheckoutPalace()
	var trippackageckt = trippackageckt.InitCheckoutTripPackage()
	group.Use(middleware.TravelerMiddleware)
	group.POST("/palace", palaceckt.CreateCheckoutPalace)
	group.POST("/trip-package", trippackageckt.CreateCheckoutTripPackage)
	
}

func NewCheckoutRouter()*CheckoutRoute{
	return &CheckoutRoute{}
}