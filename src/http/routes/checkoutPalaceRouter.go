package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/checkoutPalaceInjection"

	"github.com/labstack/echo/v4"
)

type CheckoutPalaceRoute struct {
}

func (c *CheckoutPalaceRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitCheckoutPalace()
	group.Use(middleware.AdminMiddleware)
	group.GET("", controller.GetAllCheckoutPalace)
	group.PUT("/:id", controller.UpdateCheckoutPalace)
	group.GET("/:id", controller.GetOneCheckoutPalace)
	group.DELETE("/:id", controller.DeleteCheckoutPalace)
}

func NewCheckoutPalaceRouter()*CheckoutPalaceRoute{
	return &CheckoutPalaceRoute{}
}