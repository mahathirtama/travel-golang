package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/guideInjection"

	"github.com/labstack/echo/v4"
)

type GuideRoute struct {
}

func (c *GuideRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitGuide()
	group.Use(middleware.AdminMiddleware)
	group.GET("", controller.GetAllGuide)
	group.POST("", controller.CreateGuide)
	group.PUT("/:id", controller.UpdateGuide)
	group.GET("/:id", controller.GetOneGuide)
	group.DELETE("/:id", controller.DeleteGuide)
}

func NewGuideRouter()*GuideRoute{
	return &GuideRoute{}
}