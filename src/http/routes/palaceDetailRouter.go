package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/palaceDetailInjection"

	"github.com/labstack/echo/v4"
)

type PalaceDetailRoute struct {
}

func (c *PalaceDetailRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitPalaceDetail()
	group.Use(middleware.AdminMiddleware)
	group.GET("", controller.GetAllPalaceDetail)
	group.POST("", controller.CreatePalaceDetail)
	group.DELETE("/:id", controller.DeletePalaceDetail)
}

func NewPalaceDetailRoute()*PalaceDetailRoute{
	return &PalaceDetailRoute{}
}
