package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/tripPackageInjection"

	"github.com/labstack/echo/v4"
)

type TripPackageRoute struct {
}

func (c *TripPackageRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitTripPackage()
	group.Use(middleware.AdminMiddleware)
	group.GET("", controller.GetAllTripPackage)
	group.POST("", controller.CreateTripPackage)
	group.PUT("/:id", controller.UpdateTripPackage)
	group.GET("/:id", controller.GetOneTripPackage)
	group.DELETE("/:id", controller.DeleteTripPackage)
}

func NewTripPackageRouter()*TripPackageRoute{
	return &TripPackageRoute{}
}