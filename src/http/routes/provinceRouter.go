package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/provinceInjection"

	"github.com/labstack/echo/v4"
)

type ProvinceRoute struct {
}

func (c *ProvinceRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitProvince()
	group.Use(middleware.AdminMiddleware)
	group.GET("", controller.GetAllProvince)
	group.POST("", controller.CreateProvince)
}

func NewProvinceRouter()*ProvinceRoute{
	return &ProvinceRoute{}
}
