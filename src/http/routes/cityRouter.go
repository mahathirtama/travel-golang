package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/cityInjection"

	"github.com/labstack/echo/v4"
)

type CityRoute struct {
}

func (c *CityRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitCity()
	group.Use(middleware.AdminMiddleware)
	group.GET("", controller.GetAllCity)
	group.POST("", controller.CreateCity)
}

func NewCityRouter()*CityRoute{
	return &CityRoute{}
}
