package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/palaceInjection"

	"github.com/labstack/echo/v4"
)

type PalaceRoute struct {
}

func (c *PalaceRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitPalace()
	group.Use(middleware.AdminMiddleware)
	group.GET("", controller.GetAllPalace)
	group.POST("", controller.CreatePalace)
	group.PUT("/:id", controller.UpdatePalace)
	group.GET("/:id", controller.GetOnePalace)
	group.DELETE("/:id", controller.DeletePalace)
}

func NewPalaceRouter()*PalaceRoute{
	return &PalaceRoute{}
}