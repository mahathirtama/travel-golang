package routes

import (
	midinject "travel-golang/src/app/injection/mid_inject"
	controllers "travel-golang/src/app/injection/destinationInjection"

	"github.com/labstack/echo/v4"
)

type DestinationRoute struct {
}

func (c *DestinationRoute) Router(group *echo.Group){ 
	var middleware = midinject.InitializeMiddlewareService()
	var controller = controllers.InitDestination()
	group.Use(middleware.AdminMiddleware)
	group.GET("", controller.GetAllDestination)
	group.POST("", controller.CreateDestination)
	group.PUT("/:id", controller.UpdateDestination)
	group.GET("/:id", controller.GetOneDestination)
	group.DELETE("/:id", controller.DeleteDestination)
}

func NewDestinationRouter()*DestinationRoute{
	return &DestinationRoute{}
}