package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"github.com/labstack/echo/v4"
)

type CountryControlller struct {
	CountryService *service.CountryService
}

func NewCountryController(c *service.CountryService) *CountryControlller {
	return &CountryControlller{
		c,
	}
}

func (con *CountryControlller) CreateCountry(c echo.Context) error {
	payload := con.CountryService.Create(c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *CountryControlller) GetAllCountry(c echo.Context) error {
	payload := con.CountryService.GetAll()
		apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}