package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"github.com/labstack/echo/v4"
)

type DestinationDetailController struct {
	DestinationDetailService *service.DestinationDetailService
}

func NewDestinationDetailController(s *service.DestinationDetailService) *DestinationDetailController {
	return &DestinationDetailController{
		s,
	}
}

func (con *DestinationDetailController) CreateDestinationDetail(c echo.Context) error {
	payload := con.DestinationDetailService.Create(c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *DestinationDetailController) GetAllDestinationDetail(c echo.Context) error {
	payload := con.DestinationDetailService.GetAll()
		apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *DestinationDetailController) DeleteDestinationDetail(c echo.Context) error {
	id := c.Param("id")
	payload := con.DestinationDetailService.Delete(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}