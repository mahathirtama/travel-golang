package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"github.com/labstack/echo/v4"
)

type TagController struct {
	TagService *service.TagService
}

func NewTagController(c *service.TagService) *TagController {
	return &TagController{
		c,
	}
}

func (con *TagController) CreateTag(c echo.Context) error {
	payload := con.TagService.Create(c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *TagController) GetAllTag(c echo.Context) error {
	payload := con.TagService.GetAll()
		apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}