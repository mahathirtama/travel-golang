package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"github.com/labstack/echo/v4"
)

type CheckoutTripPackageController struct {
	CheckoutTripPackageService *service.CheckoutTripPackageService
}

func NewCheckoutTripPackageController(s *service.CheckoutTripPackageService) *CheckoutTripPackageController {
	return &CheckoutTripPackageController{
		s,
	}
}

func (con *CheckoutTripPackageController) CreateCheckoutTripPackage(c echo.Context) error {
	payload, err := con.CheckoutTripPackageService.Create(c)

	if err != nil {
		apiResponse := responses.Response{
		Code: 400,
		Status: "Quota is Full",
		Data: payload,
	}

	return c.JSON(http.StatusBadRequest,apiResponse)
	}
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *CheckoutTripPackageController) GetAllCheckoutTripPackage(c echo.Context) error {
	payload := con.CheckoutTripPackageService.GetAll()
		apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *CheckoutTripPackageController) GetOneCheckoutTripPackage(c echo.Context) error {
	 id := c.Param("id")
	payload := con.CheckoutTripPackageService.GetOne(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *CheckoutTripPackageController) UpdateCheckoutTripPackage(c echo.Context) error {
	id := c.Param("id")
	payload := con.CheckoutTripPackageService.Update(id, c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}
	return c.JSON(http.StatusOK,apiResponse)
}

func (con *CheckoutTripPackageController) DeleteCheckoutTripPackage(c echo.Context) error {
	id := c.Param("id")
	payload := con.CheckoutTripPackageService.Delete(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}


