package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"github.com/labstack/echo/v4"
)

type PalaceDetailController struct {
	PalaceDetailService *service.PalaceDetailService
}

func NewPalaceDetailController(c *service.PalaceDetailService) *PalaceDetailController {
	return &PalaceDetailController{
		c,
	}
}

func (con *PalaceDetailController) CreatePalaceDetail(c echo.Context) error {
	payload := con.PalaceDetailService.Create(c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *PalaceDetailController) GetAllPalaceDetail(c echo.Context) error {
	payload := con.PalaceDetailService.GetAll()
		apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *PalaceDetailController) DeletePalaceDetail(c echo.Context) error {
	id := c.Param("id")
	payload := con.PalaceDetailService.Delete(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}
