package controllers

import (
	"fmt"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"net/http"

	"github.com/labstack/echo/v4"
)

type CityController struct {
	CityService *service.CityService
}

func NewCityController(c *service.CityService) *CityController {
	return &CityController{
		c,
	}
}


func (con *CityController) CreateCity(c echo.Context) error {
	
	payload, err := con.CityService.Create(c)
	if err != nil {
		fmt.Printf("/ncreated:%v", err)
		return err
	}
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)

}

func (con *CityController) GetAllCity(c echo.Context) error {
	payload := con.CityService.GetAll()
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}