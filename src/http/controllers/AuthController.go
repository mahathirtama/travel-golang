package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"
	"github.com/labstack/echo/v4"
)

type AuthController struct {
	jwt *service.JwtService
}

func NewAuthController(s *service.JwtService) *AuthController {
	return &AuthController{
		s,
	}
}

func (con *AuthController) Register(c echo.Context) error {
	
	payload := con.jwt.GetAuth().Create(c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)

}
func (con *AuthController) Login(c echo.Context) error {
	return con.jwt.Attempt(c)

}