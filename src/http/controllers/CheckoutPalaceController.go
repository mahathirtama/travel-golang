package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"github.com/labstack/echo/v4"
)

type CheckoutPalaceController struct {
	CheckoutPalaceService *service.CheckoutPalaceService
}

func NewCheckoutPalaceController(s *service.CheckoutPalaceService) *CheckoutPalaceController {
	return &CheckoutPalaceController{
		s,
	}
}

func (con *CheckoutPalaceController) CreateCheckoutPalace(c echo.Context) error {
	payload := con.CheckoutPalaceService.Create(c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *CheckoutPalaceController) GetAllCheckoutPalace(c echo.Context) error {
	payload := con.CheckoutPalaceService.GetAll()
		apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}


func (con *CheckoutPalaceController) GetOneCheckoutPalace(c echo.Context) error {
	 id := c.Param("id")
	payload := con.CheckoutPalaceService.GetOne(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *CheckoutPalaceController) UpdateCheckoutPalace(c echo.Context) error {
	id := c.Param("id")
	payload := con.CheckoutPalaceService.Update(id, c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}
	return c.JSON(http.StatusOK,apiResponse)
}

func (con *CheckoutPalaceController) DeleteCheckoutPalace(c echo.Context) error {
	id := c.Param("id")
	payload := con.CheckoutPalaceService.Delete(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}
