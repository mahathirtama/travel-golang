package controllers

import (
	"net/http"
	"strings"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"
	"travel-golang/src/dto"

	"github.com/labstack/echo/v4"
)

type OAuthTokenController struct {
	OAuthTokenService *service.OAuthTokenService
}

func NewOAuthTokenController(s *service.OAuthTokenService) *OAuthTokenController {
	return &OAuthTokenController{
		s,
	}
}

func (con *OAuthTokenController) CreateOAuthToken(c echo.Context) error {
	dtoRequest := dto.OAuthTokenDto{}
	_ = c.Bind(&dtoRequest)
	payload := con.OAuthTokenService.Create(dtoRequest)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)

}

func (con *OAuthTokenController) GetOneOAuthToken(c echo.Context) error {
	anyOne := c.Param("anyOne")
	column := c.Param("column")
	payload,err := con.OAuthTokenService.GetOne(column, anyOne)
	if err != nil {
		return err
	}
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *OAuthTokenController) DeleteOAuthToken(c echo.Context) error{
	authHeader := c.Request().Header.Get("Authorization")
    tokenString := strings.Split(authHeader, " ")[1]
	payload := con.OAuthTokenService.Delete(tokenString)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

