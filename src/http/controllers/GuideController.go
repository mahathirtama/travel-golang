package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"github.com/labstack/echo/v4"
)

type GuideController struct {
	GuideService *service.GuideService
}

func NewGuideController(c *service.GuideService) *GuideController {
	return &GuideController{
		c,
	}
}

func (con *GuideController) CreateGuide(c echo.Context) error {
	payload := con.GuideService.Create(c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *GuideController) GetAllGuide(c echo.Context) error {
	payload := con.GuideService.GetAll()
		apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *GuideController) GetOneGuide(c echo.Context) error {
	 id := c.Param("id")
	payload := con.GuideService.GetOne(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *GuideController) UpdateGuide(c echo.Context) error {
	id := c.Param("id")
	payload := con.GuideService.Update(id, c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}
	return c.JSON(http.StatusOK,apiResponse)
}

func (con *GuideController) DeleteGuide(c echo.Context) error {
	id := c.Param("id")
	payload := con.GuideService.Delete(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}