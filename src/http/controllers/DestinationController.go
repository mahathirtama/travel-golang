package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"github.com/labstack/echo/v4"
)

type DestinationController struct {
	DestinationService *service.DestinationService
}

func NewDestinationController(s *service.DestinationService) *DestinationController {
	return &DestinationController{
		s,
	}
}

func (con *DestinationController) CreateDestination(c echo.Context) error {
	payload := con.DestinationService.Create(c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *DestinationController) GetAllDestination(c echo.Context) error {
	payload := con.DestinationService.GetAll()
		apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *DestinationController) GetOneDestination(c echo.Context) error {
	 id := c.Param("id")
	payload := con.DestinationService.GetOne(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}


func (con *DestinationController) UpdateDestination(c echo.Context) error {
	id := c.Param("id")
	payload := con.DestinationService.Update(id, c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}
	return c.JSON(http.StatusOK,apiResponse)
}

func (con *DestinationController) DeleteDestination(c echo.Context) error {
	id := c.Param("id")
	payload := con.DestinationService.Delete(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}
