package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"github.com/labstack/echo/v4"
)

type TripAcomodationController struct {
	TripAcomodationService *service.TripAcomodationService
}

func NewTripAcomodationController(s *service.TripAcomodationService) *TripAcomodationController {
	return &TripAcomodationController{
		s,
	}
}

func (con *TripAcomodationController) CreateTripAcomodation(c echo.Context) error {
	payload := con.TripAcomodationService.Create(c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *TripAcomodationController) DeleteTripAcomodation(c echo.Context) error {
	id := c.Param("id")
	payload := con.TripAcomodationService.Delete(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}