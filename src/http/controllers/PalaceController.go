package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"github.com/labstack/echo/v4"
)

type PalaceController struct {
	PalaceService *service.PalaceService
}

func NewPalaceController(s *service.PalaceService) *PalaceController {
	return &PalaceController{
		s,
	}
}

func (con *PalaceController) CreatePalace(c echo.Context) error {
	payload := con.PalaceService.Create(c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *PalaceController) GetAllPalace(c echo.Context) error {
	payload := con.PalaceService.GetAll()
		apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *PalaceController) GetOnePalace(c echo.Context) error {
	 id := c.Param("id")
	payload := con.PalaceService.GetOne(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *PalaceController) UpdatePalace(c echo.Context) error {
	id := c.Param("id")
	payload := con.PalaceService.Update(id, c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}
	return c.JSON(http.StatusOK,apiResponse)
}

func (con *PalaceController) DeletePalace(c echo.Context) error {
	id := c.Param("id")
	payload := con.PalaceService.Delete(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}
