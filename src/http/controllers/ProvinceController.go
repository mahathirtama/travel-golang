package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"github.com/labstack/echo/v4"
)

type ProviceController struct {
	ProviceService *service.ProvinceService
}

func NewProvinceController(c *service.ProvinceService) *ProviceController {
	return &ProviceController{
		c,
	}
}

func (con *ProviceController) CreateProvince(c echo.Context) error {
	payload := con.ProviceService.Create(c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *ProviceController) GetAllProvince(c echo.Context) error {
		payload := con.ProviceService.GetAll()
		apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}