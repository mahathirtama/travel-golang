package controllers

import (
	"net/http"
	"travel-golang/src/app/responses"
	"travel-golang/src/app/service"

	"github.com/labstack/echo/v4"
)

type TripPackageController struct {
	TripPackageService *service.TripPackageService
}

func NewTripPackageController(s *service.TripPackageService) *TripPackageController {
	return &TripPackageController{
		s,
	}
}

func (con *TripPackageController) CreateTripPackage(c echo.Context) error {
	payload := con.TripPackageService.Create(c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *TripPackageController) GetAllTripPackage(c echo.Context) error {
	payload := con.TripPackageService.GetAll()
		apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *TripPackageController) GetOneTripPackage(c echo.Context) error {
	 id := c.Param("id")
	payload := con.TripPackageService.GetOne(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}

func (con *TripPackageController) UpdateTripPackage(c echo.Context) error {
	id := c.Param("id")
	payload := con.TripPackageService.Update(id, c)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}
	return c.JSON(http.StatusOK,apiResponse)
}

func (con *TripPackageController) DeleteTripPackage(c echo.Context) error {
	id := c.Param("id")
	payload := con.TripPackageService.Delete(id)
	apiResponse := responses.Response{
		Code: 200,
		Status: "OK",
		Data: payload,
	}

	return c.JSON(http.StatusOK,apiResponse)
}
