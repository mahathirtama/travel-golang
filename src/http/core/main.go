package core

import (
	"log"

	"github.com/labstack/echo/v4"
	"travel-golang/src/http/routes"
)

func Start() {
	log.Println("Server started")
	e := echo.New()
	routes.AuthRoute(e)

	city := e.Group("/api/city")
	routes.NewCityRouter().Router(city)
	

	checkoutPalace := e.Group("/api/checkout-palace")
	routes.NewCheckoutPalaceRouter().Router(checkoutPalace)
	

	checkoutTripPackage := e.Group("/api/checkout-trip-package")
	routes.NewCheckoutTripPackageRouter().Router(checkoutTripPackage)
	

	country := e.Group("/api/country")
	routes.NewCountryRoute().Router(country)
	

	destinationDetail := e.Group("/api/destination-detail")
	routes.NewDestinationDetailRoute().Router(destinationDetail)
	

	destination := e.Group("/api/destination")
	routes.NewDestinationRouter().Router(destination)
	

	guide := e.Group("/api/guide")
	routes.NewGuideRouter().Router(guide)
	

	palaceDetail := e.Group("/api/palace-detail")
	routes.NewPalaceDetailRoute().Router(palaceDetail)
	

	palace := e.Group("/api/palace")
	routes.NewPalaceRouter().Router(palace)
	

	province := e.Group("/api/province")
	routes.NewProvinceRouter().Router(province)
	

	tag := e.Group("/api/tag")
	routes.NewTagRouter().Router(tag)
	

	tripAcomodation := e.Group("/api/trip-acomodation")
	routes.NewTripAcomodationRouter().Router(tripAcomodation)
	

	tripPackage := e.Group("/api/trip-package")
	routes.NewTripPackageRouter().Router(tripPackage)

	
	checkout := e.Group("/api/checkout")
	routes.NewCheckoutRouter().Router(checkout)

	
	guest := e.Group("/api/guest")
	routes.NewGuestRoute().Router(guest)

	logout := e.Group("/api/auth")
	routes.NewOAuthRouter().Router(logout)
	
	e.Logger.Fatal(e.Start(":7000"))
	
}