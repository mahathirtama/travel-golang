package core

import (
	"flag"
	"fmt"
	"travel-golang/src/database"
)

func Flagging() {
	var migrate = flag.Bool("migrate", false, "untuk migration")
	var seed = flag.Bool("seed", false, "untuk seeder")

	flag.Parse()
	if *migrate{
		database.Migrate()
	}else if(*seed){
		fmt.Println("ini nanti isinya jalankan seeder")
	}else{
		Start()
	}
}